﻿using System;
using System.Collections.Immutable;
using System.IO;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Linq;
using System.Security.Cryptography;
using System.Collections.Generic;
using wwg.Functional;
using System.Threading;

namespace azcopycore
{
    public class BlobClient {

        CloudBlobContainer mContainer;
        public BlobClient(string connectString, string container) {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectString);
            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
            mContainer = blobClient.GetContainerReference(container);
        }

        public async Task CreateContainerAsync() {
            await mContainer.CreateIfNotExistsAsync();
        }

        public async Task<Option<byte[]>> FetchMd5Async(BlobFolder dir, string file) {
            var blobRef = mContainer.GetDirectoryReference(dir.Path).GetBlobReference(file);
            if (await blobRef.ExistsAsync()) {
                await blobRef.FetchAttributesAsync();
                return Option.Some(System.Convert.FromBase64String(blobRef.Properties.ContentMD5));
            }
            else {
                return Option.None<byte[]>();
            }
        }

        public async Task UploadFileAsync(FileInfo srcFile, BlobFolder destDir) {
            var blobRef = mContainer.GetDirectoryReference(destDir.Path).GetBlockBlobReference(srcFile.Name);
            await blobRef.UploadFromFileAsync(srcFile.FullName);
        }

        public async Task<ImmutableList<BlobFolder>> ListFoldersAsync(BlobFolder parent) {
            var folders = await mContainer.GetDirectoryReference(parent.Path).ListBlobsSegmentedAsync(null);
            return folders.Results.OfType<CloudBlobDirectory>().Select(b => new BlobFolder(b.Prefix)).ToImmutableList();
        }

        public IEnumerable<BlobFolder> ListLeafFolders(BlobFolder parent) {
            var folders = ListFoldersAsync(parent).Result;
            if (folders.Count == 0) {
                return ImmutableList.Create(parent);
            } else {
                return folders.SelectMany(ListLeafFolders);
            }
        }

        // public async Task UploadDataAsync(string filename, string data) {
        //     var blobref = mContainer.GetBlockBlobReference(filename);
        //     await blobref.UploadFromFileAsync(path);
        // }
    }

    /// represents a folder in blob storage
    public class BlobFolder {
        public string Path { get; }

        public BlobFolder() {
            this.Path = "";
        }

        public BlobFolder(string apath) {
            this.Path = apath;
        }

        public BlobFolder Append(string newpath) {
            return this.Path == "" ? new BlobFolder(newpath) : new BlobFolder(this.Path + "/" + newpath);
        }
    }

    public class UploadItem {
        public FileInfo srcFile { get; set; }
        public BlobFolder destPath { get; set; }
    }
    
    public class UploadResults {
        public ImmutableList<UploadItem> Uploads { get; set; }
    }

    public class MD5Helper {
        static MD5 md5Calc = MD5.Create();

        public static byte[] ComputeMD5(FileInfo srcFile) {
            using (var io = new FileStream(srcFile.FullName, FileMode.Open)) {
                return md5Calc.ComputeHash(io);
            }
        }
    }

    public class UploadManager {

        public static bool IsUploadFile(FileInfo fi) {
            var ext = fi.Extension.ToLowerInvariant();
            return ext == ".mp4" || ext == ".jpg" || ext == ".png" || ext == ".mov";
        }

        static string appendDestPath(string destPath, string path) {
            return destPath == "" ? path : $"{destPath}/{path}";
        }

        public static IEnumerable<UploadItem> BuildUploadList(DirectoryInfo srcDir, BlobFolder destPath) {
            var uploads = srcDir.GetFiles()
                                .Where(IsUploadFile)
                                .Select(fi => new UploadItem() {
                srcFile = fi,
                destPath = destPath
            });
            var moreUploads = srcDir.GetDirectories().SelectMany(di => BuildUploadList(di, destPath.Append(di.Name)));
            return uploads.Concat(moreUploads);
        }

        public static async Task UploadAsync(BlobClient client, UploadItem ui) {
            var srcMd5 = await Task<byte[]>.Factory.StartNew(() => MD5Helper.ComputeMD5(ui.srcFile));
            var destMd5 = await client.FetchMd5Async(ui.destPath, ui.srcFile.Name);
            bool needsUpload = destMd5.Match(md5 => srcMd5.Equals(destMd5), () => true);
            if (needsUpload) {
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine($"Upload {ui.srcFile.FullName}");
                await client.UploadFileAsync(ui.srcFile, ui.destPath);
            } else {
                Console.ForegroundColor = ConsoleColor.DarkGray;
                Console.WriteLine($"Skip {ui.srcFile.FullName}");
            }
        }
    }
    
    class Program
    {
        static async Task GenerateFolderList(BlobClient client) {
            var folders = client.ListLeafFolders(new BlobFolder());
            foreach(var f in folders) {
                Console.Out.WriteLine(f.Path);
            }
        }

        static void Upload(System.Func<BlobClient> blobClientFactory, DirectoryInfo srcDir) {
            var results = UploadManager.BuildUploadList(srcDir, new BlobFolder()).ToImmutableList();

            blobClientFactory().CreateContainerAsync().Wait();

            int totalCount = results.Count;
            Console.Out.WriteLine($"Left={totalCount}");
            Parallel.ForEach(results, 
                new ParallelOptions { MaxDegreeOfParallelism = 1 },
                ui => { 
                    UploadManager.UploadAsync(blobClientFactory(), ui).Wait(); 
                    var tot = Interlocked.Decrement(ref totalCount);
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"Left={tot}");
                } );

        }

        static void Main(string[] args)
        {
            var storageConn = args[0];
            var container = args[1];
            var op = args[2];

            if (op == "-upload") {
                var srcDir = new DirectoryInfo(args[3]);
                Upload(() => new BlobClient(storageConn, container), srcDir);
            }
            else if (op == "-index") {
                GenerateFolderList(new BlobClient(storageConn, container)).Wait();
            }

        }
    }
}
